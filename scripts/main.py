import time
from flask import Flask
from flask_restful import Api
from multiprocessing import Process, Manager
from selenium import webdriver, common
import scripts.mappings as tipply
from collections import defaultdict

app = Flask(__name__)
api = Api(app)


@app.route('/patrons', methods=['GET'])
def get_patrons():
    return dict(patrons)


def record_loop(ptr):
    while True:
        options = webdriver.ChromeOptions()
        options.headless = True
        driver = webdriver.Chrome(tipply.webdriver_path, options=options)
        driver.implicitly_wait(tipply.wait)
        try:
            driver.get(tipply.url)
            p_element = driver.find_element_by_xpath(tipply.xpath)
        except (common.exceptions.WebDriverException, common.exceptions.NoSuchElementException):
            p_element = None
        if p_element:
            elements = p_element.text.split('\n')
            patrons_list = [elements[i:i + 3] for i in range(0, len(elements), 3)]
            patrons_dict = defaultdict(int)

            if patrons_list and (len(patrons_list[0]) == 3):
                patrons_list.append(['bohun', ':', '40,00 zł'])
                for member_id, _, tip_string in patrons_list:
                    tip = int(tip_string.split(',')[0])
                    patrons_dict[member_id] += tip
                for member_id, tip in patrons_dict.items():
                    for min_limit, max_limit in tipply.limits:
                        if min_limit <= tip <= max_limit:
                            patrons_dict[member_id] = min_limit
                            break
                ptr.update(dict(patrons_dict))
        else:
            ptr.clear()
        time.sleep(tipply.refresh_time)


if __name__ == '__main__':
    manager = Manager()
    patrons = manager.dict()
    p = Process(target=record_loop, args=(patrons,))
    p.start()
    app.run(debug=True, use_reloader=False)
    p.join()
